FROM ubuntu:16.04

LABEL maintainer="cassianojaeger@gmail.com"

RUN apt-get update -y && \
    apt-get install -y python-pip python-dev curl

# We copy just the requirements.txt first to leverage Docker cache
COPY ./requirements.txt /app/requirements.txt

#
WORKDIR /app

RUN pip install -r requirements.txt

# COPYING ALL CURRENT FOLDER TO /APP
COPY . /app

# COMMAND DOCKER CONTAINER WILL TRIGGER
ENTRYPOINT [ "python" ]

# EXECUTED COMMAND AFTER RUNNING CONTAINER
CMD [ "app.py" ]