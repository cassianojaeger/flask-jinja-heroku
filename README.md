# [Flask-Jinja-Heroku](https://flask-jinja-mvc.herokuapp.com/]) ##


Flask Tutorial: https://www.freecodecamp.org/news/how-to-build-a-web-application-using-flask-and-deploy-it-to-the-cloud-3551c985e492/

Jinja2 (template language): https://pythonprogramming.net/jinja-template-flask-tutorial/
https://realpython.com/primer-on-jinja-templating/

More details: https://damyanon.net/post/flask-series-environment/
http://exploreflask.com/en/latest/templates.html
https://medium.com/the-andela-way/deploying-a-python-flask-app-to-heroku-41250bda27d0

Advanced Concepts: https://blog.syncano.rocks/advanced-concepts-flask/




## Environment Setup ##
### Requirements generation 

- **Install pipreqs:** ``pip install pipreqs``

- **Generate requirements:** ``pipreqs /flask-mvc`` 

- **To install dependencies to lib folder:**  ``pip install -t lib -r requirements.txt`` 

## Deploy with docker to heroku ##

Verify ``/deploy-heroku.sh`` file to see docker build and deploy instructions

Map **localhost:5000** to **virtualMachine:9009**, so we can forward requisitions to our docker container

``docker run -d -p 5000:9009 flask-jinja``

So, we can access our VM by accessing **localhost:5000**

Docker Tutorial: https://runnable.com/docker/python/dockerize-your-flask-application