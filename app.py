# get app variable from __init__.py file from folder app
import os
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy

from app import app

port = int(os.getenv("PORT", 9009))

# access config file by app.config["VAR_NAME"]
app.config.from_object('config')
# access getting instance config py. Sensitive information
app.config.from_pyfile('config.py')
# we need to set where he will lookup for static folder
app.static_folder = "static"
# flask login setup
login_manager = LoginManager()
login_manager.init_app(app)

basedir = os.path.abspath(os.path.dirname(__file__))
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get('DATABASE_URL') or 'sqlite:///' + os.path.join(basedir, 'app.db')
print(app.config['SQLALCHEMY_DATABASE_URI'])
db = SQLAlchemy(app)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=port, debug=app.config["DEBUG"])
