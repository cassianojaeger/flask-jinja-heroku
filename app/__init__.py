# defining the controller with its url prefix, this is how we user them
import os
from flask import Flask
from app.user.register_controller import user_management
from app.home.home import home_controller

app = Flask(__name__, instance_relative_config=True)

app.register_blueprint(user_management, url_prefix='/user')
app.register_blueprint(home_controller, url_prefix='/')

@app.context_processor
def my_utility_processor():

    def get_designer():
        """ return designer name """
        return app.config["DESIGNER"]

    return dict(designer=get_designer)