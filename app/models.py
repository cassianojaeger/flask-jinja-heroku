# models.py
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String

Base = declarative_base()

class User(Base):
    __tablename__ = 'user'
    id = Column(Integer, primary_key=True)
    email = Column(String)
    password = Column(String)
    name = Column(String)

    def __repr__(self):
        return "<User>(id='{}', email='{}', password='{}', name='{}')"\
            .format(self.id, self.email, self.password, self.name)