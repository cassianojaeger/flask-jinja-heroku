import flask
from flask import Blueprint, render_template
from app.models import User
from . import db

user_management = Blueprint('user_management', __name__, template_folder='../../templates')

@user_management.route('/', methods=['GET'])
def get_register_user():
    return render_template("register/register.html")

@user_management.route('/', methods=['POST'])
def post_register_user():
    user = User(id=000, email='cassiano@gmail.com', name='Cassiano', password='1234')
    db.session.add(user)
    db.session.commit()

# def is_safe_url(next):
#     url_for(request.args.get('next', 'index'))
#
#
# @user_management.route('/login', methods=['GET', 'POST'])
# def login():
#     # Here we use a class of some kind to represent and validate our
#     # client-side form data. For example, WTForms is a library that will
#     # handle this for us, and we use a custom LoginForm to validate.
#     form = LoginForm()
#     if form.validate_on_submit():
#         login_user(user)
#
#         flask.flash('Logged in successfully.')
#
#         next = flask.request.args.get('next')
#         if not is_safe_url(next):
#             return flask.abort(400)
#
#         return flask.redirect(next or flask.url_for('index'))
#     return flask.render_template('login.html', form=form)