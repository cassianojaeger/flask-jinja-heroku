#!/usr/bin/env bash

docker stop $(docker ps -a -q)

docker build -t flask-jinja:latest .
docker run -d -p 5000:9009 flask-jinja

heroku container:login
heroku container:push web
heroku container:release web